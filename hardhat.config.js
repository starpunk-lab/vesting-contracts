require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
const config = require("./config/config");

const mnemonicOrPrivateKey =
  "";

const bscscanApiKey = "";
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.0",
  networks: {
    rinkeby: {
      chainId: 4,
      gasPrice: 20000000000,
      url: `https://eth-rinkeby.alchemyapi.io/v2/{alchemyapi_api_key}}`,
      accounts: [mnemonicOrPrivateKey],
    },
    bsctest: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      chainId: 97,
      gasPrice: 10000000000,
      accounts: [mnemonicOrPrivateKey],
    },
    bsc: {
      url: "https://bsc-dataseed.binance.org/",
      chainId: 56,
      gasPrice: 20000000000,
      accounts: [mnemonicOrPrivateKey],
    },
    localhost: {
      url: "http://localhost:8545",
      accounts: [mnemonicOrPrivateKey],
    },
  },
  etherscan: {
    apiKey: bscscanApiKey,
  },
};
