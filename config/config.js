const beneficiaries = [
  {
    address: "",
    amount: "",
    upfrontAmount: "",
    lockDuration: "",
    vestingDuration: "",
    vestingInternal: "",
  }
];

const token = {
  name: "Token_Name",
  symbol: "Token_Symbol",
  decimals: 18,
  cap: "Cap",
};

var config = {
  beneficiaries,
  token,
};

module.exports = config;
